'''Utilities: A collection of utility functions used in the langmuir
              model and simulation software.
              Thomas K Ales, June 2018
              Iowa State Univ. of Sci. & Tech.'''
import math

def vapor_pressure(temperature):
    '''Calculates the current vapor pressure.
        Expects temperature in Kelvin, outputs pressure in Bayres.'''
    vapor_pressure_pa = math.exp(10.197 -
                                 (16211/temperature))
    vapor_pressure_bayre = vapor_pressure_pa*10
    return vapor_pressure_bayre

def taylor_hardening(taylor_factor,
                     shear_modulus=44,
                     bergers_vector=0.3,
                     dis_density=10**(13)):
    '''Calculates the contribution to YS from Taylor Hardening.
       Requires a taylor factor, optional arguements are shear modulus,
       bergers vector length (in nm) and dislocation density. If not
       specified, defaults for Ti-6Al-4V are used. Dislocation density
       is based off observed data from TEM foils.'''
    taylor_contrib = (math.sqrt(dis_density)*(
        taylor_factor*shear_modulus*bergers_vector))/(10**6)
    return taylor_contrib

def beta_rib_thickness(lath_width,
                       fv_alpha,
                       fv_equiaxed):
    '''Calculates beta rib thickness based on lath width and ratio
       of phase/equiaxed alpha'''
    beta_thickness = lath_width*(1 - (fv_alpha - fv_equiaxed))
    return beta_thickness

def hallpetch_alpha_lath(fv_colony,
                         alpha_t,
                         colony_t=0,
                         is_beta=False):
    '''Calculates the HP contribution to yield strength based on the a-lath
       geometric contributions.'''
    hp_contrib = fv_colony*150*(alpha_t**(-0.5))*beta_rib_thickness(
        alpha_t, fv_colony, 0)**0.5
    if is_beta:
        hp_contrib = hp_contrib + (fv_colony*125*alpha_t**(-0.5))
    else:
        hp_contrib = hp_contrib + (fv_colony*125*colony_t**(-0.5))
    return hp_contrib

def solution_strengthening(fv_alpha, x_Al, x_O, x_V, x_Fe):
    '''Calculates contribution from strengthening elements.'''
    aluminum = 149*(x_Al**0.667)
    oxygen = 759*(x_O**0.667)
    vanadium = (22*(x_V**0.70)**0.5)
    iron = (235*(x_Fe**0.70)**0.5)
    alpha_contrib = fv_alpha*(aluminum+oxygen)
    beta_contrib = (1-fv_alpha) * (vanadium+iron)**2
    total_contrib = alpha_contrib + beta_contrib
    return total_contrib

def base_strength(fv_alpha):
    '''Calculates the base yield strength based on phase
       fraction.'''
    base_contrib = 89*fv_alpha + 45*(1-fv_alpha)
    return base_contrib
