import math
import os

import utilities

class ThermalProfile():
    mm_cgs = 26.982
    two_pi_R = 522412300

    def __init__(self):
        pass
        
    def mass_loss(self, timestep, temperature, surf_area):
        current_pressure = utilities.vapor_pressure(temperature)
        langmuir_sqrt_term = math.sqrt(
            self.mm_cgs/(self.two_pi_R * temperature))
        massloss_flux_density = langmuir_sqrt_term*current_pressure
        massloss = massloss_flux_density*timestep*surf_area
        return massloss
