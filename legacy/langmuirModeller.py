
# coding: utf-8

# # Langmuir Modeler and YS Predictor
# Thomas K Ales | Iowa State Univ.
# March 8th 2018
# 
# This was developed to take the thermal inputs from ABAQUS and then use the calibrated langmuir emitter (n) and surface area (A) for prediction of the Aluminum content as compared to an actual build.
# 

# In[145]:


import os
import random
import math
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt


# In[2]:


# Point to a directory with all the ABAQUS thermal csvs in them.
inputCSVPath = 'T:/Abaqus CSVs/extrafine/'

csvFiles = []
# Load all files in directory that have extension .csv
for currentFile in os.listdir(inputCSVPath):
    if currentFile.endswith('.csv'):
        csvFiles.append(currentFile)
csvFiles.sort()


# In[3]:


# Create a unified dataframe of all the thermal histories and label the columns by filename. Index the rows
# by the timestamp. YOU HAVE TO HAVE "OUTPUT FOR EXACT TIME POINTS" SPECIFIED IN ABAQUS. ALSO CAPS, BECAUSE ABAQUS,
# BACK WHEN WE DIDN'T HAVE THE MEMORY FOR LOWERCASE. TWAS A SIMPLER TIME.
# Seriously, if you don't make sure the timepoints lineup, pandas will not merge the dataframes due to inconsistent
# row-indexing. You've been warned.
for csvNo, currentCSV in enumerate(csvFiles):
    if csvNo == 0:
        df_thermalProfiles = pd.read_csv(inputCSVPath + currentCSV, sep=',', header=2, index_col=0, usecols=[0,1], dtype='float64')
        df_thermalProfiles.columns = [currentCSV[:-4]]
        df_thermalProfiles.astype(float)
    else:
        thermalProfileAppend = pd.read_csv(inputCSVPath + currentCSV, sep=',', header=2, index_col=0, usecols=[0,1], dtype='float64')
        thermalProfileAppend.columns = [currentCSV[:-4]]
        thermalProfileAppend.astype(float) 
        df_thermalProfiles = pd.concat([df_thermalProfiles, thermalProfileAppend[currentCSV[:-4]]], axis=1)

print("CSVs read in...")
# In[4]:


# Plot a figure of all the thermal histories you loaded.
# plt.figure(figsize=(17,12))
# plt.plot(df_thermalProfiles)
# plt.title('Sampled Thermal Profiles for PDF')
# plt.ylabel('Temperature [K]')
# plt.xlabel('Global Sim Time [s]')
# plt.legend(df_thermalProfiles.columns)
# plt.savefig('thermalhistories.png')
# plt.show()


# In[5]:


# plt.savefig('thermalhistories.png')


# Aluminum Vapor Pressure Function (calculateVaporPress)
# $$
# p_{Al}(T) = \exp{\left(10.197 - \frac{16211}{T}\right)}
# $$
# 
# Mass Loss Equation (calculateMassLossAl)
# $$
# \dot{m} = \sqrt{\frac{MM_{Al}}{2\pi RT}} \times p_{Al}(T)
# $$
# 
# ## Put your Heat Treat (HT) information here, nerd
# Current defaults taken from,   
# "Predicting tensile properties of Ti-6Al-4V produced via directed energy deposition," B. Hayes, et. al.,  
# DOI: http://dx.doi.org/10.1016/j.actamat.2017.05.025

# In[6]:


ht_alContent = {'AMSR': 5.70, 'AMHIP': 5.74, 'AMBeta': 5.81}
ht_vContent = {'AMSR': 4.23, 'AMHIP': 4.01, 'AMBeta': 3.91}
ht_feContent = {'AMSR': 0.171, 'AMHIP': 0.172, 'AMBeta': 0.171}
ht_oxyContent = {'AMSR': 0.166, 'AMHIP': 0.167, 'AMBeta': 0.166}
ht_alphaFraction = {'AMSR': .9008, 'AMHIP': .9260, 'AMBeta': .9096}
ht_alphaColonyFrac = {'AMSR': .1780, 'AMHIP': .1937, 'AMBeta': 1.0}
ht_alphaLath = {'AMSR': 1.099, 'AMHIP': 3.533, 'AMBeta': 0.998}
ht_colonyScale = {'AMSR': 7.11, 'AMHIP': 12.41, 'AMBeta': 240.0}
ht_axisDebit = {'AMSR': {'x': -0.04, 'z': -0.08}, 'AMHIP': {'x': -0.01, 'z': -0.04}, 'AMBeta':{'x': -0.035, 'z': -0.035}}

# Create the vapor pressure function
def calculateVaporPress(temperature):
    vaporPressurePascal = math.exp(10.197 - (16211/temperature))
    vaporPressureBayre = vaporPressurePascal*10
    return vaporPressureBayre

# Create the mass-loss function
def calculateMassLossAl(timeStep, temperature, surfArea):
    vaporPressureBayre = calculateVaporPress(temperature)
    MM_Al_cgs = 26.982
    twoPiR = 522412300

    langmuirSqRt = math.sqrt(MM_Al_cgs/(twoPiR*temperature))
    massLossFluxDensity = langmuirSqRt*vaporPressureBayre
    massLoss = massLossFluxDensity*timeStep*surfArea
    return massLoss
# Taylor Hardening function
def taylorHarden(taylorFactor, 
                 shearModulus=44, 
                 bergersVector=0.3):
    dislocDensity = 10**(13)
    hardenStr = math.sqrt(dislocDensity) * (taylorFactor*shearModulus*bergersVector)
    return (hardenStr/(10**6))

# Function to estimate beta rib thickness
# Source: "Developing a phenomenoligical equation to predcit yield strength from composition and microstructure
#          in b-processed Ti-6Al-4V", Mater. Sci. Eng. A 660 (2016) 172-180
def betaRib(lathWidth, fvAlpha, fvEquiaxed):
    beta_t = lathWidth * (1-(fvAlpha - fvEquiaxed))
    return beta_t

# Calculate the hall-petch contribution from the alpha laths themselves
def HPalphaLaths(fvColonyAlpha, alphaThickness, colonyThickness=0, isBeta=False):
    hpContrib = fvColonyAlpha*150*(alphaThickness**(-0.5))*betaRib(alphaThickness, fvColonyAlpha, 0)**0.5
    if isBeta:
        hpContrib = hpContrib + (fvColonyAlpha*125*alphaThickness**(-0.5))
    else:
        hpContrib = hpContrib + (fvColonyAlpha*125*colonyThickness**(-0.5))
        
    return hpContrib



# In[7]:


# Determine the intrinisic strength of the alloy based on the phase fraction
# this returns sigma_0 in MPa, input volume fraction as a fraction of 1
def getIntrinsicStr(fvAlpha):
    yieldContribution = 89*fvAlpha + 45*(1-fvAlpha)
    return yieldContribution

# Determine the solid-solution strengthening contribution. 
# Input the Al, O, V, & Fe %wt as a fraction of 100 (e.g., 4% = 4.0 not .04)
# Returns sigma_SSS in MPa
def getSSSStr(fvAlpha, xAl, xO, xV, xFe):
    al = 149*(xAl**0.667)
    oxy = 759*(xO**0.667)
    vana = (22*(xV**0.70))**0.5
    iron = (235*(xFe**0.70))**0.5
    alphaContrib = fvAlpha*(al+oxy)
    betaContrib = (1-fvAlpha)*((vana+iron)**2)
    totalStr = alphaContrib + betaContrib
    return totalStr


# In[8]:


sArea = (0.08*0.08) # Taken from previous data. nA in research notes
elmVol = 0.1 * 0.1 * 0.1
startingAlPct = 0.066
Ti64Density_cgs = 4.43
startAlMass = elmVol*Ti64Density_cgs*startingAlPct
massTiV = elmVol*Ti64Density_cgs - (startAlMass)

finalChemistries = pd.Series(np.zeros(df_thermalProfiles.axes[1].__len__()), 
                             index=df_thermalProfiles.axes[1])
print("Doing initial calculations on supplied thermal histories")
for sampleLabel in df_thermalProfiles.axes[1]:
    tempEntry = []
    startAlMass = elmVol*Ti64Density_cgs*startingAlPct
    massTiV = elmVol*Ti64Density_cgs - (startAlMass)
    for entry, temperatureK in enumerate(df_thermalProfiles[sampleLabel]):
        if temperatureK > 1923.0:
            if entry == 1:
                tempTimeStep = df_thermalProfiles.axes[0][entry]
                massLoss = calculateMassLossAl(tempTimeStep, temperatureK, sArea)
                tempEntry = [massLoss]
            else:
                tempTimeStep = df_thermalProfiles[sampleLabel].axes[0][entry] - df_thermalProfiles[sampleLabel].axes[0][entry-1]
                massLoss = calculateMassLossAl(tempTimeStep, temperatureK, sArea)
                tempEntry.append(massLoss)
        else:
            if entry == 1:
                tempEntry = [0.0]
            else:
                tempEntry.append(0.0)
        finalAl = (startAlMass - sum(tempEntry)) / (Ti64Density_cgs*elmVol)
        finalChemistries[sampleLabel] = finalAl

# In[9]:


# plt.figure(figsize=(10, 4))
# plt.scatter(range(0, finalChemistries.axes[0].__len__()), 
#             finalChemistries)
# plt.title('Final Simulated %Wt Al')
# plt.xticks(np.arange(finalChemistries.axes[0].__len__()), finalChemistries.axes[0], rotation=60)
# plt.ylabel('wt% Aluminum')
# plt.ylim([0.040, 0.070])
# plt.show()


# In[10]:


# Assemble the dataframe of the assmebled chemistries
df_inputChemistries = pd.DataFrame(np.zeros((finalChemistries.axes[0].__len__(),17)), 
                                   index=finalChemistries.axes[0],
                                   columns = ['vf_Alpha', 'wt% Al', 'wt% V', 'wt% Fe',
                                              'wt% O', 'isSR', 'isHIP', 'isBeta', 'orientation', 
                                              'vf_Colony', 'a_thickness', 'CSF', 'sigma_0', 'sigma_SSS', 'sigma_TH',
                                              'sigma_HP', 'sigma_axisDebit'])


# In[17]:


predictedYield = pd.Series(np.zeros(finalChemistries.axes[0].__len__()),
                          index=finalChemistries.axes[0])
# Create a dataframe of the original experimental data, tag it as 'e'
for currentSample in finalChemistries.axes[0]:
    chemUncertainty = 0.035
    alphaUncertainty = 0.03
    alphaColonyUncertainty = 0.10
    lathUncertainty = 0.125
    df_inputChemistries.loc[currentSample, 'wt% Al'] = finalChemistries.loc[currentSample]*100
    df_inputChemistries.loc[currentSample, 'isSR'] = False
    df_inputChemistries.loc[currentSample, 'isHIP'] = False
    df_inputChemistries.loc[currentSample, 'isBeta'] = False
    # Assign an orientation to the sample
    orientationRoll = random.uniform(0, 1)
    
    if orientationRoll < 0.50:
        df_inputChemistries.loc[currentSample, 'orientation'] = 'x'
    else:
        df_inputChemistries.loc[currentSample, 'orientation'] = 'z'
    
    # Assign a heat treatment to the sample
    heatTreatRoll = random.uniform(0, 1)
    
    if heatTreatRoll < 0.333:
        # Mark the sample as a stress-relieved sample and compute the typical chemistry for the sample
        df_inputChemistries.loc[currentSample, 'isSR'] = True
        # Determine major intrinsic microstructure balance
        df_inputChemistries.loc[currentSample, 'vf_Alpha'] = np.random.normal(ht_alphaFraction['AMSR'],
                                                                              alphaUncertainty*ht_alphaFraction['AMSR'])
        df_inputChemistries.loc[currentSample, 'vf_Colony'] = np.random.normal(ht_alphaColonyFrac['AMSR'],
                                                                               alphaColonyUncertainty*ht_alphaColonyFrac['AMSR'])
        df_inputChemistries.loc[currentSample, 'a_thickness'] = np.random.normal(ht_alphaLath['AMSR'],
                                                                                lathUncertainty*ht_alphaLath['AMSR'])
        df_inputChemistries.loc[currentSample, 'CSF'] = np.random.normal(ht_colonyScale['AMSR'],
                                                                        alphaUncertainty*ht_colonyScale['AMSR'])
        # Generate V, O, Fe chemistries
        df_inputChemistries.loc[currentSample, 'wt% V'] = np.random.normal(ht_vContent['AMSR'],
                                                                            chemUncertainty*ht_vContent['AMSR'])
        df_inputChemistries.loc[currentSample, 'wt% Fe'] = np.random.normal(ht_feContent['AMSR'],
                                                                            chemUncertainty*ht_feContent['AMSR'])
        df_inputChemistries.loc[currentSample, 'wt% O'] = np.random.normal(ht_oxyContent['AMSR'], 
                                                                           chemUncertainty*ht_oxyContent['AMSR'])
        # Determine Intrinisic and Solid-Solution Strengthening contributions to the yield strength
        df_inputChemistries.loc[currentSample, 'sigma_0'] = getIntrinsicStr(df_inputChemistries.loc[currentSample, 'vf_Alpha'])
        df_inputChemistries.loc[currentSample, 'sigma_SSS'] = getSSSStr(df_inputChemistries.loc[currentSample, 'vf_Alpha'],
                                                                       df_inputChemistries.loc[currentSample, 'wt% Al'],
                                                                        df_inputChemistries.loc[currentSample, 'wt% O'],
                                                                        df_inputChemistries.loc[currentSample, 'wt% V'],
                                                                        df_inputChemistries.loc[currentSample, 'wt% Fe'])
        
        # Determine taylor hardening contribution, Hall-Petch contribution
        df_inputChemistries.loc[currentSample, 'sigma_TH'] = taylorHarden(3.2)
        df_inputChemistries.loc[currentSample, 'sigma_HP'] = HPalphaLaths(df_inputChemistries.loc[currentSample, 'vf_Colony'],
                                                                         df_inputChemistries.loc[currentSample, 'a_thickness'],
                                                                         df_inputChemistries.loc[currentSample, 'CSF'])
        if(df_inputChemistries.loc[currentSample, 'sigma_HP'] == 'NaN'):
            df_inputChemistries.loc[currentSample, 'sigma_HP'] = 0.0
            
        if df_inputChemistries.loc[currentSample, 'orientation'] == 'x':
            df_inputChemistries.loc[currentSample, 'sigma_axisDebit'] = ht_axisDebit['AMSR']['x']*(df_inputChemistries.loc[currentSample, 'sigma_0'] +
                                                                                                   df_inputChemistries.loc[currentSample, 'sigma_SSS'] +
                                                                                                   df_inputChemistries.loc[currentSample, 'sigma_TH'] +
                                                                                                   df_inputChemistries.loc[currentSample, 'sigma_HP'])
        else:
            df_inputChemistries.loc[currentSample, 'sigma_axisDebit'] = ht_axisDebit['AMSR']['x']*(df_inputChemistries.loc[currentSample, 'sigma_0'] +
                                                                                                   df_inputChemistries.loc[currentSample, 'sigma_SSS'] +
                                                                                                   df_inputChemistries.loc[currentSample, 'sigma_TH'] +
                                                                                                   df_inputChemistries.loc[currentSample, 'sigma_HP'])
            
    elif 0.334 < heatTreatRoll < 0.666: 
        # Determine major intrinsic microstructure balance
        df_inputChemistries.loc[currentSample, 'isHIP'] = True
        df_inputChemistries.loc[currentSample, 'vf_Alpha'] = np.random.normal(ht_alphaFraction['AMHIP'],
                                                                              alphaUncertainty*ht_alphaFraction['AMHIP'])
        df_inputChemistries.loc[currentSample, 'vf_Colony'] = np.random.normal(ht_alphaColonyFrac['AMHIP'],
                                                                               alphaColonyUncertainty*ht_alphaColonyFrac['AMHIP'])
        if df_inputChemistries.loc[currentSample, 'vf_Colony'] > 1.00000:
            df_inputChemistries.loc[currentSample, 'vf_Colony'] = 1.0
            
        df_inputChemistries.loc[currentSample, 'a_thickness'] = np.random.normal(ht_alphaLath['AMHIP'],
                                                                                lathUncertainty*ht_alphaLath['AMHIP'])
        df_inputChemistries.loc[currentSample, 'CSF'] = np.random.normal(ht_colonyScale['AMHIP'],
                                                                        alphaUncertainty*ht_colonyScale['AMHIP'])
        # Generate V, O, Fe chemistries
        df_inputChemistries.loc[currentSample, 'wt% V'] = np.random.normal(ht_vContent['AMHIP'],
                                                                           chemUncertainty*ht_vContent['AMHIP'])
        df_inputChemistries.loc[currentSample, 'wt% Fe'] = np.random.normal(ht_feContent['AMHIP'],
                                                                           chemUncertainty*ht_feContent['AMHIP'])
        df_inputChemistries.loc[currentSample, 'wt% O'] = np.random.normal(ht_oxyContent['AMHIP'], 
                                                                          chemUncertainty*ht_oxyContent['AMHIP'])
        # Determine Intrinisic and Solid-Solution Strengthening contributions to the yield strength
        df_inputChemistries.loc[currentSample, 'sigma_0'] = getIntrinsicStr(df_inputChemistries.loc[currentSample, 'vf_Alpha'])
        df_inputChemistries.loc[currentSample, 'sigma_SSS'] = getSSSStr(df_inputChemistries.loc[currentSample, 'vf_Alpha'],
                                                                       df_inputChemistries.loc[currentSample, 'wt% Al'],
                                                                        df_inputChemistries.loc[currentSample, 'wt% O'],
                                                                        df_inputChemistries.loc[currentSample, 'wt% V'],
                                                                        df_inputChemistries.loc[currentSample, 'wt% Fe'])
        
        # Determine taylor hardening contribution, Hall-Petch contribution
        df_inputChemistries.loc[currentSample, 'sigma_TH'] = 0.0
        df_inputChemistries.loc[currentSample, 'sigma_HP'] = HPalphaLaths(df_inputChemistries.loc[currentSample, 'vf_Colony'],
                                                                         df_inputChemistries.loc[currentSample, 'a_thickness'],
                                                                         df_inputChemistries.loc[currentSample, 'CSF'])
            
        if df_inputChemistries.loc[currentSample, 'orientation'] == 'x':
            df_inputChemistries.loc[currentSample, 'sigma_axisDebit'] = ht_axisDebit['AMHIP']['x']*(df_inputChemistries.loc[currentSample, 'sigma_0'] +
                                                                                                   df_inputChemistries.loc[currentSample, 'sigma_SSS'] +
                                                                                                   df_inputChemistries.loc[currentSample, 'sigma_TH'] +
                                                                                                   df_inputChemistries.loc[currentSample, 'sigma_HP'])
        else:
            df_inputChemistries.loc[currentSample, 'sigma_axisDebit'] = ht_axisDebit['AMHIP']['x']*(df_inputChemistries.loc[currentSample, 'sigma_0'] +
                                                                                                   df_inputChemistries.loc[currentSample, 'sigma_SSS'] +
                                                                                                   df_inputChemistries.loc[currentSample, 'sigma_TH'] +
                                                                                                   df_inputChemistries.loc[currentSample, 'sigma_HP'])
            
    else:
        df_inputChemistries.loc[currentSample, 'isBeta'] = True
        df_inputChemistries.loc[currentSample, 'vf_Alpha'] = np.random.normal(ht_alphaFraction['AMBeta'],
                                                                              alphaUncertainty*ht_alphaFraction['AMBeta'])
        df_inputChemistries.loc[currentSample, 'vf_Colony'] = np.random.normal(ht_alphaColonyFrac['AMBeta'],
                                                                               alphaColonyUncertainty*ht_alphaColonyFrac['AMHIP'])
        if df_inputChemistries.loc[currentSample, 'vf_Colony'] > 1.00000:
            df_inputChemistries.loc[currentSample, 'vf_Colony'] = 1.0
            
        df_inputChemistries.loc[currentSample, 'a_thickness'] = np.random.normal(ht_alphaLath['AMBeta'],
                                                                                lathUncertainty*ht_alphaLath['AMBeta'])
        df_inputChemistries.loc[currentSample, 'CSF'] = np.random.normal(ht_colonyScale['AMBeta'],
                                                                        alphaUncertainty*ht_colonyScale['AMBeta'])
        # Generate V, O, Fe chemistries
        df_inputChemistries.loc[currentSample, 'wt% V'] = np.random.normal(ht_vContent['AMBeta'],
                                                                           chemUncertainty*ht_vContent['AMBeta'])
        df_inputChemistries.loc[currentSample, 'wt% Fe'] = np.random.normal(ht_feContent['AMBeta'],
                                                                           chemUncertainty*ht_feContent['AMBeta'])
        df_inputChemistries.loc[currentSample, 'wt% O'] = np.random.normal(ht_oxyContent['AMBeta'], 
                                                                          chemUncertainty*ht_oxyContent['AMBeta'])
        # Determine Intrinisic and Solid-Solution Strengthening contributions to the yield strength
        df_inputChemistries.loc[currentSample, 'sigma_0'] = getIntrinsicStr(df_inputChemistries.loc[currentSample, 'vf_Alpha'])
        df_inputChemistries.loc[currentSample, 'sigma_SSS'] = getSSSStr(df_inputChemistries.loc[currentSample, 'vf_Alpha'],
                                                                       df_inputChemistries.loc[currentSample, 'wt% Al'],
                                                                        df_inputChemistries.loc[currentSample, 'wt% O'],
                                                                        df_inputChemistries.loc[currentSample, 'wt% V'],
                                                                        df_inputChemistries.loc[currentSample, 'wt% Fe'])
        
        # Determine taylor hardening contribution, Hall-Petch contribution
        df_inputChemistries.loc[currentSample, 'sigma_TH'] = 0.0
        df_inputChemistries.loc[currentSample, 'sigma_HP'] = HPalphaLaths(df_inputChemistries.loc[currentSample, 'vf_Colony'],
                                                                         df_inputChemistries.loc[currentSample, 'a_thickness'],
                                                                         df_inputChemistries.loc[currentSample, 'CSF'])
        if(df_inputChemistries.loc[currentSample, 'sigma_HP'] == 'NaN'):
            df_inputChemistries.loc[currentSample, 'sigma_HP'] = 0.0
            
        if df_inputChemistries.loc[currentSample, 'orientation'] == 'x':
            df_inputChemistries.loc[currentSample, 'sigma_axisDebit'] = ht_axisDebit['AMBeta']['x']*(df_inputChemistries.loc[currentSample, 'sigma_0'] +
                                                                                                   df_inputChemistries.loc[currentSample, 'sigma_SSS'] +
                                                                                                   df_inputChemistries.loc[currentSample, 'sigma_TH'] +
                                                                                                   df_inputChemistries.loc[currentSample, 'sigma_HP'])
        else:
            df_inputChemistries.loc[currentSample, 'sigma_axisDebit'] = ht_axisDebit['AMBeta']['x']*(df_inputChemistries.loc[currentSample, 'sigma_0'] +
                                                                                                   df_inputChemistries.loc[currentSample, 'sigma_SSS'] +
                                                                                                   df_inputChemistries.loc[currentSample, 'sigma_TH'] +
                                                                                                   df_inputChemistries.loc[currentSample, 'sigma_HP'])
    
    predictedYield.loc[currentSample] = (df_inputChemistries.loc[currentSample, 'sigma_0'] + 
                                          df_inputChemistries.loc[currentSample, 'sigma_SSS'] + 
                                          df_inputChemistries.loc[currentSample, 'sigma_TH'] +
                                          df_inputChemistries.loc[currentSample, 'sigma_HP'] +
                                          df_inputChemistries.loc[currentSample, 'sigma_axisDebit'])

print("Calculations done.")

# In[18]:


# plt.figure(figsize=(10,5))
# plt.scatter(range(0, predictedYield.axes[0].__len__()),
#                   predictedYield)
# plt.xlabel('Sample')
# plt.xticks(np.arange(finalChemistries.axes[0].__len__()), finalChemistries.axes[0], rotation=60)
# plt.ylabel(r'$\sigma_{y}$ [MPa]')
# plt.ylim([400, 1200])
# plt.title(r'Predicted $\sigma_{y}$:' + '\n' + r'axisDebit( $\sigma_{0} + \sigma_{SSS} + \sigma_{TH} + \sigma_{HP}$ )')
# plt.show()
df_inputChemistries.to_csv('E:/predictedyield.csv')
print("Input data and calculated YS written out as CSV.")

# ## Monte Carlo Section
# In this portion of the code, we begin incorporating the various measurement errors into the synthetic dataset so we can build a proper probability distribution function.
# 
# The original distribution is shown below, there are also several configuration options in the code below:
# 

# In[140]:

print("Starting Monte Carlos...")
# Permuations per point
permutations = 10000
# Uncertainties
jiggle = {'AMSR':{'vf_Alpha': 0.021, 'vf_Colony': 0.1, 'chem': 0.035},
         'AMHIP':{'vf_Alpha': 0.021, 'vf_Colony': 0.1, 'chem': 0.035},
         'AMBeta':{'vf_Alpha': 0.021, 'vf_Colony': 0.1, 'chem': 0.035}}


# In[141]:


df_inputChemistries.axes[0].__len__()*permutations
columnList = ['sample', 'vf_Alpha', 'wt% Al', 'wt% V',
             'wt% Fe', 'wt% O', 'isSR', 'isHIP', 'isBeta', 'orientation',
             'vf_Colony', 'a_thickness', 'CSF', 'sigma_0', 'sigma_SSS',
             'sigma_TH', 'sigma_HP', 'sigma_axisDebit', 'sigma_ys']
perPrefix = 'monte-'
permList = []
for seq in range(0, permutations*df_inputChemistries.axes[0].__len__()):
    permList.append(perPrefix + '{:06d}'.format(seq))
# Create the permutation dataframe
df_monte = pd.DataFrame(np.zeros((df_inputChemistries.axes[0].__len__()*permutations,
                                columnList.__len__())), index=permList, columns=columnList)


# In[142]:


for offset, currentSample in enumerate(df_inputChemistries.axes[0]):
    sampleName = 'monte-' + '{:06d}'.format((offset)*permutations)
    print(sampleName)
    # Copy the original point over
    tempList = df_inputChemistries.loc[currentSample].tolist()
    tempList.insert(0, currentSample)
    tempList.append(predictedYield.loc[currentSample])
    df_monte.loc[sampleName] = tempList
    if df_inputChemistries.loc[currentSample, 'isSR']:
        heat = 'AMSR'
    elif df_inputChemistries.loc[currentSample, 'isHIP']:
        heat = 'AMHIP'
    else:
        heat = 'AMBeta'
    
    for currentMutation in range(0, permutations):
        sampleName = 'monte-' + '{:06d}'.format((offset)*permutations + currentMutation)
        print('Running Monte Carlo Calculation '+sampleName+'\\n')
        # Set the sample name that this is a variation of
        df_monte.loc[sampleName, 'sample'] = currentSample
        df_monte.loc[sampleName, 'orientation'] = df_inputChemistries.loc[currentSample, 'orientation']
        # Tweak the microstructures
        df_monte.loc[sampleName, 'vf_Alpha'] = np.random.normal(df_inputChemistries.loc[currentSample, 'vf_Alpha'],
                                                               df_inputChemistries.loc[currentSample, 'vf_Alpha']*\
                                                                                       jiggle[heat]['vf_Alpha'])
        if(df_monte.loc[sampleName, 'vf_Alpha']) > 1.000:
            df_monte.loc[sampleName, 'vf_Alpha'] = 1.0
        
        df_monte.loc[sampleName, 'vf_Colony'] = np.random.normal(df_inputChemistries.loc[currentSample, 'vf_Colony'],
                                                               df_inputChemistries.loc[currentSample, 'vf_Colony']*\
                                                                                       jiggle[heat]['vf_Colony'])
        if(df_monte.loc[sampleName, 'vf_Colony']) > 1.000:
            df_monte.loc[sampleName, 'vf_Colony'] = 1.0
            
        df_monte.loc[sampleName, 'a_thickness'] = np.random.normal(df_inputChemistries.loc[currentSample, 'a_thickness'],
                                                                  df_inputChemistries.loc[currentSample, 'a_thickness']*0.10)
        
        df_monte.loc[sampleName, 'CSF'] = df_inputChemistries.loc[currentSample, 'CSF']
        

        # Copy the heat treat information
        df_monte.loc[sampleName, 'isSR'] = df_inputChemistries.loc[currentSample, 'isSR']
        df_monte.loc[sampleName, 'isHIP'] = df_inputChemistries.loc[currentSample, 'isHIP']
        df_monte.loc[sampleName, 'isBeta'] = df_inputChemistries.loc[currentSample, 'isBeta']
        
        # Tweak the chemistry
        df_monte.loc[sampleName, 'wt% Al'] = np.random.normal(df_inputChemistries.loc[currentSample, 'wt% Al'],
                                                             df_inputChemistries.loc[currentSample, 'wt% Al']*jiggle[heat]['chem'])
        df_monte.loc[sampleName, 'wt% V'] = np.random.normal(df_inputChemistries.loc[currentSample, 'wt% V'],
                                                            df_inputChemistries.loc[currentSample, 'wt% V']*jiggle[heat]['chem'])
        df_monte.loc[sampleName, 'wt% O'] = np.random.normal(df_inputChemistries.loc[currentSample, 'wt% O'],
                                                            df_inputChemistries.loc[currentSample, 'wt% O']*jiggle[heat]['chem'])
        
        df_monte.loc[sampleName, 'wt% Fe'] = np.random.normal(df_inputChemistries.loc[currentSample, 'wt% Fe'],
                                                             df_inputChemistries.loc[currentSample, 'wt% Fe']*jiggle[heat]['chem'])
        
        # Calcluate intrinsic and SSS for new chemistry + microstructure
        df_monte.loc[sampleName, 'sigma_0'] = getIntrinsicStr(df_monte.loc[sampleName, 'vf_Alpha'])
        df_monte.loc[sampleName, 'sigma_SSS'] = getSSSStr(df_monte.loc[sampleName, 'vf_Alpha'], df_monte.loc[sampleName, 'wt% Al'],
                                                         df_monte.loc[sampleName, 'wt% O'],
                                                          df_monte.loc[sampleName, 'wt% V'], 
                                                          df_monte.loc[sampleName, 'wt% Fe'])
        # Calculate the Hall-Petch Contribution
        df_monte.loc[sampleName, 'sigma_HP'] = HPalphaLaths(df_monte.loc[sampleName, 'vf_Colony'], df_monte.loc[sampleName, 'a_thickness'],
                                                           df_monte.loc[sampleName, 'CSF'])
        # Is the sample AMSR? If so, add in the taylor hardening contribution
        if df_monte.loc[sampleName, 'isSR']:
            df_monte.loc[sampleName, 'sigma_TH'] = taylorHarden(3.2)
        else:
            df_monte.loc[sampleName, 'sigma_TH'] = 0.0
        
        df_monte.loc[sampleName, 'sigma_axisDebit'] = (df_monte.loc[sampleName, 'sigma_0'] + df_monte.loc[sampleName, 'sigma_SSS'] + df_monte.loc[sampleName, 'sigma_HP'] + df_monte.loc[sampleName, 'sigma_TH'])*ht_axisDebit[heat][df_monte.loc[sampleName, 'orientation']]
        df_monte.loc[sampleName, 'sigma_ys'] = (df_monte.loc[sampleName, 'sigma_0'] + df_monte.loc[sampleName, 'sigma_SSS'] +                                               df_monte.loc[sampleName, 'sigma_HP'] + df_monte.loc[sampleName, 'sigma_TH'] +                                               df_monte.loc[sampleName, 'sigma_axisDebit'])
print("Writing out monte carlo results")
df_monte.to_csv('E:/montecarloresults.csv')
print("done.")
