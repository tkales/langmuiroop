""" Langmuir GUI and Control Code
    Thomas K Ales, May 2018
    Iowa State University of Sci. & Tech."""
import sys
import os
from PyQt5.QtWidgets import QApplication, QMainWindow, QFileDialog
from langmUIr import Ui_langmUIr

ht_vContent = {'AMSR': 4.23, 'AMHIP': 4.01, 'AMBeta': 3.91}
ht_feContent = {'AMSR': 0.171, 'AMHIP': 0.172, 'AMBeta': 0.171}
ht_oxyContent = {'AMSR': 0.166, 'AMHIP': 0.167, 'AMBeta': 0.166}
ht_alphaFraction = {'AMSR': .9008, 'AMHIP': .9260, 'AMBeta': .9096}
ht_alphaColonyFrac = {'AMSR': .1780, 'AMHIP': .1937, 'AMBeta': 1.0}
ht_alphaLath = {'AMSR': 1.099, 'AMHIP': 3.533, 'AMBeta': 0.998}
ht_colonyScale = {'AMSR': 7.11, 'AMHIP': 12.41, 'AMBeta': 240.0}
ht_axisDebit = {'AMSR': {'x': -0.04, 'z': -0.08}, 
                'AMHIP': {'x': -0.01, 'z': -0.04}, 
                'AMBeta':{'x': -0.035, 'z': -0.035}}

def read_thermals(fileDirectory):
    tmp_filelist = []
    for current_file in os.listdir(fileDirectory):
        if current_file.endswith('csv'):
            tmp_filelist.append(current_file)
            print(current_file)

    return tmp_filelist

def calculateMCRuns(mcMultiplier, numberOfCSVFiles):
    required_runs = mcMultiplier*numberOfCSVFiles
    return required_runs

class LangmuirGUI(QMainWindow):
    # Default Monte Carlo sampling fractions.
    monteCarloFractions = {"x": 0.50, "y": 0.25, "z": 0.25}
    startAlContent = 6.50
    startAlloyDensity = 4.43
    emitterPrefactor = 1.0
    emitterCharacteristicLength = 0.001
    emitterClampingTemperature = 700.0
    threadsForSimulation = 1
    knockdowns = {"x": 0.08, "y": 0.06, "z": 0.18}
    variances = {"chemistry": 0.035, "lath": 0.075, "volumeFraction": 0.010}

    def __init__(self):

        super(LangmuirGUI, self).__init__()
        self.ui = Ui_langmUIr()
        self.ui.setupUi(self)
        # Setup required connection callbacks
        self.ui.btn_selectCSVFolder.clicked.connect(self.loadThermals)
        self.ui.btn_clearAllCSVs.clicked.connect(self.clearThermals)
        self.ui.btn_mcOutputDirectory.clicked.connect(self.selectOutputDirectory)
        self.ui.sim_execute.clicked.connect(self.startSimulation)

        # Populate default values
        self.ui.chem_alloyDensity.setText(str(self.startAlloyDensity))
        self.ui.chem_startAl.setText(str(self.startAlContent))
        self.ui.chem_emitterPrefactor.setText(str(self.emitterPrefactor))
        self.ui.chem_clampTemp.setText(str(self.emitterClampingTemperature))
        self.ui.chem_domainSideLength.setText(str(self.emitterCharacteristicLength))
        self.ui.sim_threads.setValue(self.threadsForSimulation)
        
        # Instance Variables
        self.currentCSVDirectory = ""           # Placeholder for path to CSV files
        self.currentCSVFiles = []               # List of CSV files in directory
        self.monteCarloMultiplier = 1000        # Number of MC runs to do per CSV
        self.requiredMCRuns = 0
        self.outputDirectory = ""               # Destination directory for MC files

        # Populate the Monte Carlo GUI Widgets with the sampling distribution defaults
        self.ui.mc_xFraction.insert('{:.2f}'.format(self.monteCarloFractions["x"]))
        self.ui.mc_yFraction.insert('{:.2f}'.format(self.monteCarloFractions["y"]))
        self.ui.mc_zFraction.insert('{:.2f}'.format(self.monteCarloFractions["z"]))
        self.ui.mc_sampleMultiplier.insert(str(self.monteCarloMultiplier))
        self.ui.mc_totalRunsReqd.setText(str(self.requiredMCRuns))
        self.ui.knockdown_x.setText('{:.2f}'.format(self.knockdowns["x"]))
        self.ui.knockdown_y.setText('{:.2f}'.format(self.knockdowns["y"]))
        self.ui.knockdown_z.setText('{:.2f}'.format(self.knockdowns["z"]))
        self.ui.cov_alphaFraction.setText('{:.3f}'.format(self.variances["volumeFraction"]))
        self.ui.cov_alphaLath.setText('{:.3f}'.format(self.variances["lath"]))
        self.ui.cov_chemistry.setText('{:.3f}'.format(self.variances["chemistry"]))
        

    def loadThermals(self):
        """ Present a QFileDialog to select thermal CSV directory, populate thermalCSVListWidget
            and update the currentCSVDirectory and currentCSVFiles instance variables.
        """
        self.clearThermals()
        file = str(QFileDialog.getExistingDirectory(self, "Select Directory"))
        currentCSVDirectory = file
        currentCSVFiles = read_thermals(currentCSVDirectory)
        currentCSVFiles.sort()
        print(currentCSVFiles.__len__())
        # Update the list widget with the CSV files.
        print([currentCSVDirectory + "/" + f for f in currentCSVFiles])
        self.ui.thermalCSVListWidget.addItems([currentCSVDirectory + "/" + f for f in currentCSVFiles])   
        
        # Calculate the required # of runs and update the label accordingly.
        self.ui.mc_totalRunsReqd.setText(str(
            calculateMCRuns(self.monteCarloMultiplier, currentCSVFiles.__len__())))

        return

    def clearThermals(self):
        self.currentCSVDirectory = ""
        self.currentCSVFiles = []
        self.ui.thermalCSVListWidget.clear()
        return

    def selectOutputDirectory(self):
        outputDir = str(QFileDialog.getExistingDirectory(
            self, "Select Output Directory"))
        self.ui.lbl_currentOutputDirectory.setText(str(outputDir))

    def startSimulation(self):
        print("Simulation Started...")
        return 1


if __name__ == "__main__":
    app = QApplication(sys.argv)
    langmuirwindow = LangmuirGUI()
    langmuirwindow.show()
    app.exec_()
